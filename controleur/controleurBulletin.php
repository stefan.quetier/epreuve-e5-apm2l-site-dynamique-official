<?php
$_SESSION['listeBulletin'] = new lesBulletinDTO(bulletinDAO::lesBulletins());



if(isset($_GET['bulletin'])){
	$_SESSION['bulletin'] = $_GET['bulletin'];
  
    
}
else {
    if (!isset($_SESSION['bulletin'])) {
        $_SESSION['bulletin'] = 0;

    }
}
if(isset($_POST["ajouterBulletin"])){
    $_SESSION['bulletin'] = 0;

}

if(isset($_POST["anullerBulletin"])){
    $_SESSION["bulletin"]=$_SESSION['listeBulletin']->premierContrat();
}

if(isset($_POST["enregistrerUtilisateur"])){
    $reponseSGBD = bulletinDAO::BulletinModif($_POST["IDBULLETIN"],$_POST["IDCONTRAT"],$_POST["MOIS"],$_POST["ANNEE"]);
    if($reponseSGBD){
        $_SESSION['listeBulletin'] = new lesBulletinDTO(bulletinDAO::lesBulletins());
    }
    else{
        echo "Error";
    }
}

if(isset($_POST["ajouterBulletinBDD"])){
    $reponseSGBD = bulletinDAO::BulletinAJout($_POST["numBulletin"],$_POST["numContrat"],$_POST["moisBulletin"],$_POST["annéeBulletin"],$_POST["file"]);
    if($reponseSGBD){
        $_SESSION['listeBulletin'] = new lesBulletinDTO(bulletinDAO::lesBulletins());
    }
    else{
        echo "Error";
    }
}
$bulletinActif = $_SESSION['listeBulletin']->chercheBulletin($_SESSION['bulletin']);
if(isset($_POST["supprimerBulletin"])){
    $reponseSGBD=bulletinDAO::BulletinSupprimer($bulletinActif->getIdContrat());

    if($reponseSGBD){
        $_SESSION['listeBulletin']=new lesBulletinDTO(bulletinDAO::lesBulletins());
        $_SESSION['bulletin']=$_SESSION['listeBulletin']->premierContrat();
    }

}




$formulaireGestion = new formulaire('post', 'index.php', 'fBulletin', 'fBulletin');
if(utilisateurDAO::getFonctionbyLogin($_SESSION["identification"])[0] === 'ressource_humaine'){


    if($bulletinActif!==null){
        if (isset($_POST["modifierBulletin"])){
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('N°Bulletin : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("IDBULLETIN", "IDBULLETIN", $bulletinActif->getIDBULLETIN(), "1", "", "0"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('N°Contrat : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("IDCONTRAT", "IDCONTRAT", $bulletinActif->getIDCONTRAT(), "1", "", "0"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Mois : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("MOIS", "MOIS", $bulletinActif->getMOIS(), "1", "", "0"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Année : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("ANNEE", "ANNEE", $bulletinActif->getANNEE(), "1", "", "0"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Fichier Exel : '),1);
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabelClick("fichier/MODELES_De_Fiches_de_Paie.pdf", "pdfBulletin", "Ouvrir le fichier"), 1);
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("enregistrerUtilisateur","enregistrerUtilisateur","Enregistrer"));
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("anullerBulletin","AnullerBulletin","Annuler"))  ;
            $formulaireGestion->ajouterComposantTab();
        }else{
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('N°Bulletin : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numBulletin", "numBulletin", $bulletinActif->getIDBULLETIN(), "1", "", "1"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('N°Contrat : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numContrat", "numContrat", $bulletinActif->getIDCONTRAT(), "1", "", "1"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Mois : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("moisBulletin", "moisBulletin", $bulletinActif->getMOIS(), "1", "", "1"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Année : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("annéeBulletin", "annéeBulletin", $bulletinActif->getANNEe(), "1", "", "1"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Fichier Exel : '),1);
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabelClick("fichier/BulletinDePaie_" . $bulletinActif->getIDBULLETIN() . ".pdf", "pdfBulletin", "Ouvrir le fichier"), 1);
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("modifierBulletin","ModifierBulletin","Modifier"));
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("ajouterBulletin","AjouterBulletin","Ajouter"));
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("supprimerBulletin","SupprimerBulletin","Supprimer"))  ;
            $formulaireGestion->ajouterComposantTab();
        }

    }
    else{
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('N°Bulletin : '),1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numBulletin", "numBulletin","", "0", "", "0"), 1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('N°Contrat : '),1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numContrat", "numContrat", "", "0", "", "0"), 1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Mois : '),1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("moisBulletin", "moisBulletin", "", "0", "", "0"), 1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Année : '),1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("annéeBulletin", "annéeBulletin","", "0", "", "0"), 1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Fichier Exel : '),1);
        $formulaireGestion->ajouterComposantTab();

        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputFile("file", "inputFile", "fichier/ "), 1);
        $formulaireGestion->ajouterComposantTab();

        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("ajouterBulletinBDD","AjouterUtilisateurBDD","Ajouter"));
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("anullerBulletin","anullerUtilisateur","Annuler"))  ;
        $formulaireGestion->ajouterComposantTab();
    }


}
else{
    $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Il faut un autre compte.'),1);
    $formulaireGestion->ajouterComposantTab();
}
$formulaireGestion->creerFormulaire();



require_once 'vue/vueBulletin.php';
