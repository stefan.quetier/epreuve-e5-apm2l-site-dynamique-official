<?php


$_SESSION['listeFonction'] = new lesFonctionDTO(fonctionDAO::lesFonctions());


if (isset($_GET['fonction'])) {
    $_SESSION['fonction'] = $_GET['fonction'];


} else {
    if (!isset($_SESSION['fonction'])) {
        $_SESSION['fonction'] = 0;

    }
}
if(isset($_POST["ajouterFonction"])){
    $_SESSION['fonction'] = 0;

}

if(isset($_POST["anullerFonction"])){
    $_SESSION["fonction"]=$_SESSION['listeFonction']->premiereFonction();
}
if(isset($_POST["enregistrerFonction"])){
    $reponseSGBD = fonctionDAO::FonctionModif($_POST["numFonction"],$_POST["libelle"]);
    if($reponseSGBD){
        $_SESSION['listeFonction'] = new lesFonctionDTO(fonctionDAO::lesFonctions());
    }
    else{
        echo "Error";
    }
}
if(isset($_POST["ajouterFonctionBDD"])){
    $reponseSGBD = fonctionDAO::FonctionAjout($_POST["numFonction"],$_POST["libelle"]);
    if($reponseSGBD){
        $_SESSION['listeFonction'] = new lesFonctionDTO(fonctionDAO::lesFonctions());
    }
    else{
        echo "Error";
    }
}
$fonctionActif = $_SESSION['listeFonction']->chercheFonction($_SESSION['fonction']);
if(isset($_POST["supprimerFonction"])) {
    $reponseSGBD = fonctionDAO::FonctionSupprimer($fonctionActif->getIDFONCTION());
    if ($reponseSGBD) {
        $_SESSION['listeFonction'] = new lesFonctionDTO(fonctionDAO::lesFonctions());
        $_SESSION['fonction'] = $_SESSION['listeFonction']->premiereFonction();
    }
}

    $formulaireGestion = new formulaire('post', 'index.php', 'fBulletin', 'fBulletin');
    if (utilisateurDAO::getFonctionbyLogin($_SESSION["identification"])[0] === 'ressource_humaine') {
        if ($fonctionActif !== null) {
            if (isset($_POST["modifierFonction"])) {

                $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('N°Fonction : '), 1);
                $formulaireGestion->ajouterComposantTab();
                $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numFonction", "NumFonction", $fonctionActif->getIDFONCTION(), "1", "", "0"), 1);
                $formulaireGestion->ajouterComposantTab();
                $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Libelle de la fonction :   '), 1);
                $formulaireGestion->ajouterComposantTab();
                $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("libelle", "Libelle", $fonctionActif->getLIBELLE(), "1", "", "0"), 1);
                $formulaireGestion->ajouterComposantTab();
                $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("enregistrerFonction", "EnregistrerFonction", "Enregistrer"));
                $formulaireGestion->ajouterComposantTab();

                $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("anullerFonction", "AnullerFonction", "Annuler"));
                $formulaireGestion->ajouterComposantTab();
            } else {
                $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('N°Fonction : '), 1);
                $formulaireGestion->ajouterComposantTab();
                $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numFonction", "NumFonction", $fonctionActif->getIDFONCTION(), "1", "", "1"), 1);
                $formulaireGestion->ajouterComposantTab();
                $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Libelle de la fonction :   '), 1);
                $formulaireGestion->ajouterComposantTab();
                $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numUser", "NumUser", $fonctionActif->getLIBELLE(), "1", "", "1"), 1);
                $formulaireGestion->ajouterComposantTab();


                $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("modifierFonction", "ModifierFonction", "Modifier"));
                $formulaireGestion->ajouterComposantTab();

                $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("ajouterFonction", "AjouterFonction", "Ajouter"));
                $formulaireGestion->ajouterComposantTab();

                $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("supprimerFonction", "SupprimerFonction", "Supprimer"));
                $formulaireGestion->ajouterComposantTab();
            }


        } else {
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('N°Fonction : '), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numFonction", "NumFonction", "", "0", "", "0"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Libelle de la fonction :   '), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("libelle", "Libelle", "", "0", "", "0"), 1);
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("ajouterFonctionBDD", "AjouterFonctionBDD", "Ajouter"));
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("anullerFonction", "AnullerFonction", "Annuler"));
            $formulaireGestion->ajouterComposantTab();
        }


    } else {
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Il faut un autre compte.'), 1);
        $formulaireGestion->ajouterComposantTab();
    }
    $formulaireGestion->creerFormulaire();

    require_once 'vue/vueFonction.php';


