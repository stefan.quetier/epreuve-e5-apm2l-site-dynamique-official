<?php

$_SESSION['listeintervenant'] = new lesUtilisateursDTO(UtilisateurDAO::LesIntervenants());

$menuIntervenant = new Menu("menuIntervannt");


if(isset($_GET['utilisateur'])){
	$_SESSION['utilisateur'] = $_GET['utilisateur'];   
}
else
{
	if(!isset($_SESSION['utilisateur'])){
		$_SESSION['utilisateur']= 0;
       
	}
}

if(isset($_POST["ajouterUtilisateur"])){
    $_SESSION["utilisateur"]=0;

}


if(isset($_POST["anullerUtilisateur"])){
	$_SESSION["utilisateur"]=$_SESSION['listeintervenant']->premierUtilisateur();
}





// echo "<br>";

if(isset($_POST["ajouterUtilisateurBDD"])){
	$reponseSGBD = UtilisateurDAO::UtilisateurAjouter($_POST["idUser"],$_POST["idFonction"],$_POST["idLigue"],$_POST["idClub"],$_POST["nom"],$_POST["prenom"],$_POST["statut"]);
	if($reponseSGBD){
		$_SESSION['listeintervenant'] = new LesutilisateursDTO(utilisateurDAO::LesIntervenants());
	}
    else{
        echo "Error";
    }
}
if(isset($_POST["enregistrerIntervenant"])){
    $reponseSGBD = utilisateurDAO::UtilisateurModif($_POST["idUser"],$_POST["idlibelle"],$_POST["idLigue"],$_POST["idClub"],$_POST["nom"],$_POST["prenom"],$_POST["statut"]);
    if($reponseSGBD){
        $_SESSION['listeBulletin'] = new lesBulletinDTO(bulletinDAO::lesBulletins());
    }
    else{
        echo "Error";
    }
}


$utilisateurActif = $_SESSION['listeintervenant']->chercheUtilisateur($_SESSION['utilisateur']);


if(isset($_POST["supprimerUtilisateur"])){
	$reponseSGBD=UtilisateurDAO::UtilisateurSupprimer($_SESSION['utilisateurActif']->getIdUser());
	if($reponseSGBD){
		$_SESSION['listeintervenant']=new LesUtilisateursDTO(UtilisateurDAO::LesIntervenants());
		$_SESSION['utilisateur']=$_SESSION['listeintervenant']->premierUtilisateur();
	}
	
}




$formulaireGestion = new formulaire('post', 'index.php', 'fIntervenant', 'fIntervenant');

if(utilisateurDAO::getFonctionbyLogin($_SESSION["identification"]) == "responsable_formation"){
    $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Identifiant :', "z"),1);
    $formulaireGestion->ajouterComposantTab();
}

if(utilisateurDAO::getFonctionbyLogin($_SESSION["identification"]) == "secretaire"){
    $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Identifiant :', "z"),1);
    $formulaireGestion->ajouterComposantTab();
}


if(utilisateurDAO::getFonctionbyLogin($_SESSION["identification"])[0] == "ressource_humaine"){
   
    if ($_SESSION['utilisateur']!=0){

        if (isset($_POST["modifierUtilisateur"])){
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Nom Intervenant : '),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("nom", "Nom", $utilisateurActif->getNom(),"1" , "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Prenom Intervenant : '),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("prenom", "Prenom",$utilisateurActif->getPrenom(),"1" , "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Id utilisateur: '),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("idUser", "idUser",$utilisateurActif->getIdUser(),"1" , "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();

            

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Ligue Intervenant et id ligue  : ',),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("nomLigue", "NomLigue",$utilisateurActif->getNomLigue(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("idLigue", "idLigue",$utilisateurActif->getIdLigue(),"1" , "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();
            

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Club Intervenant et id club : ',),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("nomClub", "NomClub",$utilisateurActif->getNomClub(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("idClub", "idClub",$utilisateurActif->getIdClub(),"1" , "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Fonction et id fonction : ',),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("libelle", "Libelle",$utilisateurActif->getLibelle(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("idlibelle", "idlibelle",$utilisateurActif->getIdFonction(),"1" , "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Statut : ',),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("statut", "statut",$utilisateurActif->getStatut(),"1" , "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();
            /*
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Contrat : ',),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("contrat", "contrat","1" ,"", "", "0") , 1 );
            $formulaireGestion->ajouterComposantTab();*/

      
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("enregistrerIntervenant","EnregistrerIntervenant","Enregistrer"))  ;
            $formulaireGestion->ajouterComposantTab(); 

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("anullerUtilisateur","AnullerUtilisateur","Annuler"))  ;
            $formulaireGestion->ajouterComposantTab(); 



        }
        else{
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Nom Intervenant : '),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("nom", "nom", $utilisateurActif->getNom(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Prenom Intervenant : '),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("prenom", "prenom",$utilisateurActif->getPrenom(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantTab();



            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Ligue Intervenant : ',),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("prenom", "prenom",$utilisateurActif->getNomLigue(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Club Intervenant : ',),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("prenom", "prenom",$utilisateurActif->getNomClub(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Fonction : ',),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("prenom", "prenom",$utilisateurActif->getLibelle(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Statut Intervenant : '),1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("prenom", "prenom",$utilisateurActif->getStatut(),"1" , "", "1") , 1 );
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("modifierUtilisateur","ModifierUtilisateur","Modifier"));
            $formulaireGestion->ajouterComposantTab();  

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("ajouterUtilisateur","AjouterUtilisateur","Ajouter"));
            $formulaireGestion->ajouterComposantTab(); 

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("supprimerUtilisateur","SupprimerUtilisateur","Supprimer"))  ;
            $formulaireGestion->ajouterComposantTab(); 


        }
       
    }   
    else{
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel("Veuillez remplir les champs pour crée un utilisateur :  "),1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel("Id de l'intervenant : "),1);
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("idUser", "idUser", "","0" , "", "0") , 1 );
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('ID Fonction : ',),1);
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte(" idFonction", "idFonction","","0" , "", "0") , 1 );
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('ID Ligue : ',),1);
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("idLigue", "idLigue","","0" , "", "0") , 1 );
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('ID Club : ',),1);
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("idClub", "idClub","","0" , "", "0") , 1 );
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Nom Intervenant : '),1);
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("nom", "nom", "","0" , "", "0") , 1 );
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Prenom Intervenant : '),1);
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("prenom", "prenom", "","0" , "", "0") , 1 );
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Statut : '),1);
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("statut", "statut","","0" , "", "0") , 1 );
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("ajouterUtilisateurBDD","AjouterUtilisateurBDD","Ajouter"));
        $formulaireGestion->ajouterComposantTab(); 
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("anullerUtilisateur","anullerUtilisateur","Annuler"))  ;
        $formulaireGestion->ajouterComposantTab(); 


    } 
    
}

$formulaireGestion->creerFormulaire();

require_once 'vue/vueIntervenant.php';