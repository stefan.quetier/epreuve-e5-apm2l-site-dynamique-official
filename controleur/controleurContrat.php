<?php
$_SESSION['listeContrat'] = new lesContratDTO(contratDAO::lesContrats());



if(isset($_GET['contrat'])){
    $_SESSION['contrat'] = $_GET['contrat'];


}
else
{
    if(!isset($_SESSION['contrat'])){
        $_SESSION['contrat']= 0;

    }
}
if(isset($_POST["ajouterContrat"])){
    $_SESSION['contrat'] = 0;

}

if(isset($_POST["anullerContrat"])){
    $_SESSION["contrat"]=$_SESSION['listeContrat']->premierContrat();
}


if(isset($_POST["ajouterContratBDD"])){
    $reponseSGBD = contratDAO::ContratAjout($_POST["numContrat"],$_POST["numUser"],$_POST["annéeDebut"],$_POST["annéeFin"],$_POST["typeContrat"],$_POST["heureContrat"]);
    if($reponseSGBD){
        $_SESSION['listeBulletin'] = new lesBulletinDTO(bulletinDAO::lesBulletins());
    }
    else{
        echo "Error";
    }
}

if(isset($_POST["enregistrerContrat"])){
    $reponseSGBD = contratDAO::ContratModfif($_POST["numContrat"],$_POST["numUser"],$_POST["annéeDebut"],$_POST["annéeFin"],$_POST["typeContrat"],$_POST["heureContrat"]);
    if($reponseSGBD){
        $_SESSION['listeBulletin'] = new lesBulletinDTO(bulletinDAO::lesBulletins());
    }
    else{
        echo "Error";
    }
}
$contratActif = $_SESSION['listeContrat']->chercheContrat($_SESSION['contrat']);

if(isset($_POST["supprimerContrat"])){
    $reponseSGBD=contratDAO::ContratSupprimer($contratActif->getIdContrat());
    if($reponseSGBD){
        $_SESSION['listeBulletin']=new lesContratDTO(contratDAO::lesContrats());
        $_SESSION['bulletin']=$_SESSION['listeBulletin']->premierContrat();
    }

}
$formulaireGestion = new formulaire('post', 'index.php', 'fBulletin', 'fBulletin');





if(utilisateurDAO::getFonctionbyLogin($_SESSION["identification"])[0] === 'ressource_humaine'){
    if($contratActif !== null){
        if (isset($_POST["modifierContrat"])){
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('N°Contrat : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numContrat", "NumContrat", $contratActif->getIdContrat(), "1", "", "0"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Nom et id user  '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numUser", "NumUser", $contratActif->getNom(), "1", "", "1"), 1);
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numUser", "NumUser", $contratActif->getIdUser(), "1", "", "0"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Année de debut du contrat : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("annéeDebut", "AnnéeDebut", $contratActif->getDATEDEBUT(), "1", "", "0"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Année de fin du contrat : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("annéeFin", "AnnéeFin", $contratActif->getDATEFIN(), "1", "", "0"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Type du contrat : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("typeContrat", "TypeContrat", $contratActif->getTYPECONTRAT(), "1", "", "0"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Nombre d heure du contrat : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("heureContrat", "HeureContrat", $contratActif->getNBHEURES(), "1", "", "0"), 1);
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("enregistrerContrat","EnregistrerContrat","Enregistrer"));
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("anullerContrat","AnullerContrat","Annuler"))  ;
            $formulaireGestion->ajouterComposantTab();
        }else {
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('N°Contrat : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numContrat", "numContrat", $contratActif->getIdContrat(), "1", "", "1"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Nom utilisateur : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numUser", "NumUser", $contratActif->getNom(), "1", "", "1"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Année de debut du contrat : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("annéeDebut", "AnnéeDebut", $contratActif->getDATEDEBUT(), "1", "", "1"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Année de fin du contrat : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("annéeFin", "AnnéeDebut", $contratActif->getDATEFIN(), "1", "", "1"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Type du contrat : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("typeContrat", "TypeContrat", $contratActif->getTYPECONTRAT(), "1", "", "1"), 1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Nombre d heure du contrat : '),1);
            $formulaireGestion->ajouterComposantTab();
            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("heureContrat", "HeureContrat", $contratActif->getNBHEURES(), "1", "", "1"), 1);
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("modifierContrat","ModifierContrat","Modifier"));
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("ajouterContrat","AjouterContrat","Ajouter"));
            $formulaireGestion->ajouterComposantTab();

            $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("supprimerContrat","SupprimerContrat","Supprimer"))  ;
            $formulaireGestion->ajouterComposantTab();
        }



    }else{
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('N°Contrat : '),1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numContrat", "numContrat", "", "0", "", "0"), 1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('User Id : '),1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("numUser", "NumUser", "", "0", "", "0"), 1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Année de debut du contrat : '),1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("annéeDebut", "moisBulletin", "", "0", "", "0"), 1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Année de fin du contrat : '),1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("annéeFin", "AnnéeFin","", "0", "", "0"), 1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Type du contrat : '),1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("typeContrat", "TypeContrat", "", "0", "", "0"), 1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Nombre d heure du contrat : '),1);
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputTexte("heureContrat", "HeureContrat","", "0", "", "0"), 1);
        $formulaireGestion->ajouterComposantTab();

        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("ajouterContratBDD","AjouterContratBDD","Ajouter"));
        $formulaireGestion->ajouterComposantTab();
        $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerInputSubmit("anullerContrat","AnullerContrat","Annuler"))  ;
        $formulaireGestion->ajouterComposantTab();
    }


}
else{
    $formulaireGestion->ajouterComposantLigne($formulaireGestion->creerLabel('Il faut un autre compte.'),1);
    $formulaireGestion->ajouterComposantTab();
}
$formulaireGestion->creerFormulaire();



require_once 'vue/vueContrat.php';