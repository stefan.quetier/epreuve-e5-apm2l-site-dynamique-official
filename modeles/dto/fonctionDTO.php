<?php
class fonctionDTO{
    use Hydrate;
    private ?string $IDFONCTION;
 	private ?string $LIBELLE;
    private ?string $NOM;

    /**
     * @param string|null $IDFONCTION
     * @param string|null $LIBELLE
     * @param string|null $NOM
     */
    public function __construct(?string $IDFONCTION, ?string $LIBELLE, ?string $NOM)
    {
        $this->IDFONCTION = $IDFONCTION;
        $this->LIBELLE = $LIBELLE;
        $this->NOM = $NOM;
    }

    public function getIDFONCTION(): ?string
    {
        return $this->IDFONCTION;
    }

    public function setIDFONCTION(?string $IDFONCTION): void
    {
        $this->IDFONCTION = $IDFONCTION;
    }

    public function getLIBELLE(): ?string
    {
        return $this->LIBELLE;
    }

    public function setLIBELLE(?string $LIBELLE): void
    {
        $this->LIBELLE = $LIBELLE;
    }

    public function getNOM(): ?string
    {
        return $this->NOM;
    }

    public function setNOM(?string $NOM): void
    {
        $this->NOM = $NOM;
    }




}