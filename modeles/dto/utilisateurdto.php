<?php
class UtilisateurDTO{
    use Hydrate;
	private ?string $idUser;
	private ?string $idFonction;
	private ?int $IDLIGUE;
	private ?int $IDCLUB;
	private ?string $nom;
	private ?string $prenom;
	private ?string $login;
	private ?string $mdp;
	private ?string $statut;
    private ?string $libelle;
    private ?string $nomLigue;
    private ?string $nomClub;

    /**
     * @param string|null $idUser
     * @param string|null $idFonction
     * @param int|null $IDLIGUE
     * @param int|null $IDCLUB
     * @param string|null $nom
     * @param string|null $prenom
     * @param string|null $login
     * @param string|null $mdp
     * @param string|null $statut
     * @param string|null $libelle
     * @param string|null $nomLigue
     * @param string|null $nomClub
     */
    public function __construct(?string $idUser, ?string $idFonction, ?int $IDLIGUE, ?int $IDCLUB, ?string $nom, ?string $prenom, ?string $login, ?string $mdp, ?string $statut, ?string $libelle, ?string $nomLigue, ?string $nomClub)
    {
        $this->idUser = $idUser;
        $this->idFonction = $idFonction;
        $this->IDLIGUE = $IDLIGUE;
        $this->IDCLUB = $IDCLUB;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->login = $login;
        $this->mdp = $mdp;
        $this->statut = $statut;
        $this->libelle = $libelle;
        $this->nomLigue = $nomLigue;
        $this->nomClub = $nomClub;
    }

    public function getIdUser(): ?string
    {
        return $this->idUser;
    }

    public function setIdUser(?string $idUser): void
    {
        $this->idUser = $idUser;
    }

    public function getIdFonction(): ?string
    {
        return $this->idFonction;
    }

    public function setIdFonction(?string $idFonction): void
    {
        $this->idFonction = $idFonction;
    }

    public function getIDLIGUE(): ?int
    {
        return $this->IDLIGUE;
    }

    public function setIDLIGUE(?int $IDLIGUE): void
    {
        $this->IDLIGUE = $IDLIGUE;
    }

    public function getIDCLUB(): ?int
    {
        return $this->IDCLUB;
    }

    public function setIDCLUB(?int $IDCLUB): void
    {
        $this->IDCLUB = $IDCLUB;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): void
    {
        $this->nom = $nom;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(?string $prenom): void
    {
        $this->prenom = $prenom;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(?string $login): void
    {
        $this->login = $login;
    }

    public function getMdp(): ?string
    {
        return $this->mdp;
    }

    public function setMdp(?string $mdp): void
    {
        $this->mdp = $mdp;
    }

    public function getStatut(): ?string
    {
        return $this->statut;
    }

    public function setStatut(?string $statut): void
    {
        $this->statut = $statut;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function getNomLigue(): ?string
    {
        return $this->nomLigue;
    }

    public function setNomLigue(?string $nomLigue): void
    {
        $this->nomLigue = $nomLigue;
    }

    public function getNomClub(): ?string
    {
        return $this->nomClub;
    }

    public function setNomClub(?string $nomClub): void
    {
        $this->nomClub = $nomClub;
    }




}

