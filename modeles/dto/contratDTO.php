<?php
class contratDTO{
    use Hydrate;
	private ?string  $IDCONTRAT;
	private ?string  $IDUSER ;
    private ?string  $DateDebut;
    private ?string  $DateFin;
    private ?string  $TypeContrat;
    private ?int  $NbHeures;
    private ?string $nom;

    /**
     * @param string|null $IDCONTRAT
     * @param string|null $IDUSER
     * @param string|null $DateDebut
     * @param string|null $DateFin
     * @param string|null $TypeContrat
     * @param int|null $NbHeures
     * @param string|null $nom
     */
    public function __construct(?string $IDCONTRAT, ?string $IDUSER, ?string $DateDebut, ?string $DateFin, ?string $TypeContrat, ?int $NbHeures, ?string $nom)
    {
        $this->IDCONTRAT = $IDCONTRAT;
        $this->IDUSER = $IDUSER;
        $this->DateDebut = $DateDebut;
        $this->DateFin = $DateFin;
        $this->TypeContrat = $TypeContrat;
        $this->NbHeures = $NbHeures;
        $this->nom = $nom;
    }

    public function getIDCONTRAT(): ?string
    {
        return $this->IDCONTRAT;
    }

    public function setIDCONTRAT(?string $IDCONTRAT): void
    {
        $this->IDCONTRAT = $IDCONTRAT;
    }

    public function getIDUSER(): ?string
    {
        return $this->IDUSER;
    }

    public function setIDUSER(?string $IDUSER): void
    {
        $this->IDUSER = $IDUSER;
    }

    public function getDateDebut(): ?string
    {
        return $this->DateDebut;
    }

    public function setDateDebut(?string $DateDebut): void
    {
        $this->DateDebut = $DateDebut;
    }

    public function getDateFin(): ?string
    {
        return $this->DateFin;
    }

    public function setDateFin(?string $DateFin): void
    {
        $this->DateFin = $DateFin;
    }

    public function getTypeContrat(): ?string
    {
        return $this->TypeContrat;
    }

    public function setTypeContrat(?string $TypeContrat): void
    {
        $this->TypeContrat = $TypeContrat;
    }

    public function getNbHeures(): ?int
    {
        return $this->NbHeures;
    }

    public function setNbHeures(?int $NbHeures): void
    {
        $this->NbHeures = $NbHeures;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(?string $nom): void
    {
        $this->nom = $nom;
    }




}

    


