<?php
class lesFonctionDTO {
    private array $fonction ;


    public function __construct($array){
        if (is_array($array)) {
            $this->fonction = $array;
        }
    }

    public function getlesFonctions(){
        return $this->fonction;
    }

    public function chercheFonction($uneFonctionId){
        $i = 0;
        while ($uneFonctionId != $this->fonction[$i]->getIDFONCTION() && $i < count($this->fonction)-1){
            $i++;
        }
        if ($uneFonctionId == $this->fonction[$i]->getIDFONCTION()){
            return $this->fonction[$i];
        }
    }

    public function premiereFonction(){
        return $this->fonction[0]->getIDFONCTION();
    }
}