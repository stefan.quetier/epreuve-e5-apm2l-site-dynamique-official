<?php
class bulletinDTO{
    use Hydrate;
	private ?string  $IDBULLETIN;
	private ?string  $IDCONTRAT;
    private ?string  $MOIS ;
    private ?string  $ANNEE;
    private ?string  $BULLETINPDF;

    /**
     * @param string|null $IDBULLETIN
     * @param string|null $IDCONTRAT
     * @param string|null $MOIS
     * @param string|null $ANNEE
     * @param string|null $BULLETINPDF
     */
    public function __construct(?string $IDBULLETIN, ?string $IDCONTRAT, ?string $MOIS, ?string $ANNEE, ?string $BULLETINPDF)
    {
        $this->IDBULLETIN = $IDBULLETIN;
        $this->IDCONTRAT = $IDCONTRAT;
        $this->MOIS = $MOIS;
        $this->ANNEE = $ANNEE;
        $this->BULLETINPDF = $BULLETINPDF;
    }

    public function getIDBULLETIN(): ?string
    {
        return $this->IDBULLETIN;
    }

    public function setIDBULLETIN(?string $IDBULLETIN): void
    {
        $this->IDBULLETIN = $IDBULLETIN;
    }

    public function getIDCONTRAT(): ?string
    {
        return $this->IDCONTRAT;
    }

    public function setIDCONTRAT(?string $IDCONTRAT): void
    {
        $this->IDCONTRAT = $IDCONTRAT;
    }

    public function getMOIS(): ?string
    {
        return $this->MOIS;
    }

    public function setMOIS(?string $MOIS): void
    {
        $this->MOIS = $MOIS;
    }

    public function getANNEE(): ?string
    {
        return $this->ANNEE;
    }

    public function setANNEE(?string $ANNEE): void
    {
        $this->ANNEE = $ANNEE;
    }

    public function getBULLETINPDF(): ?string
    {
        return $this->BULLETINPDF;
    }

    public function setBULLETINPDF(?string $BULLETINPDF): void
    {
        $this->BULLETINPDF = $BULLETINPDF;
    }




}

    


