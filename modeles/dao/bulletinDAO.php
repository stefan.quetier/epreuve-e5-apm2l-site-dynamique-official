<?php
class bulletinDAO {
    public static function lesBulletins(){
        $result = [];
        $requetePrepa = dBConnex::getInstance()->prepare("select * from `bulletin`" );

        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        if(!empty($liste)){
            foreach($liste as $intervenant){
                $unIntervenant = new bulletinDTO(null,null,null,null,null);
                $unIntervenant->hydrate($intervenant);
                $result[] = $unIntervenant;
            }
        }
        return $result;
    }

    public static function BulletinModif($IDBULLETIN, $IDCONTRAT, $MOIS, $ANNEE) {
        $requetePrepa = DBConnex::getInstance()->prepare("UPDATE `bulletin` 
                                                          SET IDBULLETIN = :IDBULLETIN,
                                                              MOIS = :MOIS,
                                                              ANNEE = :ANNEE
                                                          WHERE IDCONTRAT = :IDCONTRAT");
        $requetePrepa->bindParam(':IDBULLETIN', $IDBULLETIN);
        $requetePrepa->bindParam(':IDCONTRAT', $IDCONTRAT);
        $requetePrepa->bindParam(':MOIS', $MOIS);
        $requetePrepa->bindParam(':ANNEE', $ANNEE);

        $requetePrepa->execute();
    }




    public static function BulletinAJout($IDBULLETIN, $IDCONTRAT, $MOIS, $ANNEE,$BULLETINPDF) {
        $requetePrepa = DBConnex::getInstance()->prepare("INSERT INTO bulletin (IDBULLETIN, IDCONTRAT, MOIS, ANNEE,BULLETINPDF) 
                                                      VALUES (:IDBULLETIN, :IDCONTRAT, :MOIS, :ANNEE,:BULLETINPDF)");
        $requetePrepa->bindParam(':IDBULLETIN', $IDBULLETIN);
        $requetePrepa->bindParam(':IDCONTRAT', $IDCONTRAT);
        $requetePrepa->bindParam(':MOIS', $MOIS);
        $requetePrepa->bindParam(':ANNEE', $ANNEE);
        $requetePrepa->bindParam(':BULLETINPDF', $BULLETINPDF);

        $requetePrepa->execute();
    }

    public static function BulletinSupprimer($IDCONTRAT){
        $requetePrepa=DBConnex::getInstance()->prepare("DELETE FROM bulletin WHERE IDCONTRAT=:IDCONTRAT");
        $requetePrepa->bindParam(':IDCONTRAT', $IDCONTRAT);
        return $requetePrepa->execute();
    }
}