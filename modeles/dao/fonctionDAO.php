<?php
class fonctionDAO
{
    public static function lesFonctions()
    {
        $result = [];
        $requetePrepa = dBConnex::getInstance()->prepare("SELECT fonction.*,utilisateur.NOM
                                                                FROM fonction,utilisateur
                                                                WHERE fonction.IDFONCTION=utilisateur.IDFONCTION;");

        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($liste)) {
            foreach ($liste as $intervenant) {
                $unIntervenant = new fonctionDTO(null, null, null);
                $unIntervenant->hydrate($intervenant);
                $result[] = $unIntervenant;
            }
        }
        return $result;
    }

    public static function FonctionAjout($IDFONCTION, $LIBELLE) {
        $requetePrepa = DBConnex::getInstance()->prepare("INSERT INTO fonction (IDFONCTION, LIBELLE) 
                                                  VALUES (:IDFONCTION, :LIBELLE)");
        $requetePrepa->bindParam(':IDFONCTION', $IDFONCTION);
        $requetePrepa->bindParam(':LIBELLE', $LIBELLE);

        $requetePrepa->execute();
    }

    public static function  FonctionSupprimer($IDFONCTION){
        $requetePrepa=DBConnex::getInstance()->prepare("DELETE FROM fonction WHERE IDFONCTION=:IDFONCTION");
        $requetePrepa->bindParam(':IDFONCTION', $IDFONCTION);
        return $requetePrepa->execute();
    }

    public static function FonctionModif($IDFONCTION, $LIBELLE) {
        $requetePrepa = DBConnex::getInstance()->prepare("UPDATE fonction 
                                                          SET LIBELLE = :LIBELLE 
                                                          WHERE IDFONCTION = :IDFONCTION");
        $requetePrepa->bindParam(':IDFONCTION', $IDFONCTION);
        $requetePrepa->bindParam(':LIBELLE', $LIBELLE);
    
        $requetePrepa->execute();
    }
    

}
