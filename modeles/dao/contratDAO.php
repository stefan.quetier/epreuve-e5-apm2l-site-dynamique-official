<?php
class contratDAO {
    public static function lesContrats(){
        $result = [];
        $requetePrepa = dBConnex::getInstance()->prepare("select contrat.*,utilisateur.nom 
                                                                from contrat, utilisateur 
                                                                where contrat.IDUSER=utilisateur.IDUSER;" );

        $requetePrepa->execute();
        $liste = $requetePrepa->fetchAll(PDO::FETCH_ASSOC);

        if(!empty($liste)){
            foreach($liste as $intervenant){
                $unIntervenant = new contratDTO(null, null, null ,null,null ,null,null );
                $unIntervenant->hydrate($intervenant);
                $result[] = $unIntervenant;
            }
        }
        return $result;
    }

    public static function ContratAjout($IDCONTRAT,$IDUSER,$DATEDEBUT, $DATEFIN,$TYPECONTRAT,$NBHEURES) {
        $requetePrepa = DBConnex::getInstance()->prepare("INSERT INTO contrat (IDCONTRAT, IDUSER, DATEDEBUT, DATEFIN, TYPECONTRAT, NBHEURES) 
                                                      VALUES (:IDCONTRAT, :IDUSER, :DATEDEBUT, :DATEFIN, :TYPECONTRAT, :NBHEURES)");
        $requetePrepa->bindParam(':IDCONTRAT', $IDCONTRAT);
        $requetePrepa->bindParam(':IDUSER', $IDUSER);
        $requetePrepa->bindParam(':DATEDEBUT', $DATEDEBUT);
        $requetePrepa->bindParam(':DATEFIN', $DATEFIN);
        $requetePrepa->bindParam(':TYPECONTRAT', $TYPECONTRAT);
        $requetePrepa->bindParam(':NBHEURES', $NBHEURES);

        $requetePrepa->execute();
    }
    public static function ContratModfif($IDCONTRAT, $IDUSER, $DATEDEBUT, $DATEFIN, $TYPECONTRAT, $NBHEURES) {
        $requetePrepa = DBConnex::getInstance()->prepare("UPDATE contrat 
                                                      SET IDUSER = :IDUSER,
                                                          DATEDEBUT = :DATEDEBUT,
                                                          DATEFIN = :DATEFIN,
                                                          TYPECONTRAT = :TYPECONTRAT,
                                                          NBHEURES = :NBHEURES
                                                      WHERE IDCONTRAT = :IDCONTRAT");
        $requetePrepa->bindParam(':IDCONTRAT', $IDCONTRAT);
        $requetePrepa->bindParam(':IDUSER', $IDUSER);
        $requetePrepa->bindParam(':DATEDEBUT', $DATEDEBUT);
        $requetePrepa->bindParam(':DATEFIN', $DATEFIN);
        $requetePrepa->bindParam(':TYPECONTRAT', $TYPECONTRAT);
        $requetePrepa->bindParam(':NBHEURES', $NBHEURES);

        $requetePrepa->execute();
    }


    public static function ContratSupprimer($IDCONTRAT){
        $requetePrepa=DBConnex::getInstance()->prepare("DELETE FROM contrat WHERE IDCONTRAT=:IDCONTRAT");
        $requetePrepa->bindParam(':IDCONTRAT', $IDCONTRAT);
        return $requetePrepa->execute();
    }

    public static function getFonctionbyStatut($unLogin){
        $result = [];
        $requeteprera = DBConnex::getInstance()->prepare(" SELECT utilisateur.statut FROM fonction, utilisateur WHERE LOGIN = :login AND utilisateur.IDFONCTION = fonction.IDFONCTION ");
        $requeteprera->bindParam( ":login", $unLogin);

        $requeteprera->execute();

        $login = $requeteprera->fetchAll(PDO::FETCH_NUM);

        return $login[0];
    }



}