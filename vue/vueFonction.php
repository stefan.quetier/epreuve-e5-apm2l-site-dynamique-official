<div class="conteneur">
    <header>
        <?php include 'haut.php' ;?>
    </header>
    <main>
        <div class='gauche'>
            <?php include 'vue/fonction/vueFonctionGauche.php' ;?>
        </div>
        <div class='droiteee'>
            <?php include 'vue/fonction/vueFonctionDroit.php' ;?>
        </div>
    </main>
    <footer>
        <?php include 'bas.php' ;?>
    </footer>
</div>