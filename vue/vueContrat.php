<div class="conteneur">
	<header>
		<?php include 'haut.php' ;?>
	</header>
	<main>
        <div class='gauche'>
            <?php include 'vue/contrat/vueContratGauche.php' ;?>
        </div>
        <div class='droite'>
            <?php include 'vue/contrat/vueContratDroit.php' ;?>
        </div>
    </main>
	<footer>
		<?php include 'bas.php' ;?>
	</footer>
</div>